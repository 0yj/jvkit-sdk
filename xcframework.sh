# Make this executable using:
# > chmod u+x xcframework.sh
# Execute it using:
# ./xcframework.sh
#
PROJECT_NAME="JVKit"
WORKPLACE="Example/${PROJECT_NAME}.xcworkspace"
IOS_SIM="Archive/simulator.xcarchive"
IOS_DEV="Archive/iphoneos.xcarchive"
FRAMEWORK_PATH="Products/Library/Frameworks/${PROJECT_NAME}.framework"

# Archive for simulator
xcodebuild archive \
-workspace "${WORKPLACE}" \
-scheme "${PROJECT_NAME}" \
-destination "generic/platform=iOS Simulator" \
-archivePath "${IOS_SIM}" \
SKIP_INSTALL=NO \
BUILD_LIBRARY_FOR_DISTRIBUTION=YES &&

# Archive for iphoneos
xcodebuild archive \
-workspace "${WORKPLACE}" \
-scheme "${PROJECT_NAME}" \
-destination "generic/platform=iOS" \
-archivePath "${IOS_DEV}" \
SKIP_INSTALL=NO \
BUILD_LIBRARY_FOR_DISTRIBUTION=YES &&

# Remove existing xcframework
rm -rf "${PROJECT_NAME}/Frameworks/" &&

# Creation of xcframework
xcodebuild -create-xcframework \
-framework "${IOS_SIM}/${FRAMEWORK_PATH}" \
-framework "${IOS_DEV}/${FRAMEWORK_PATH}" \
-output "${PROJECT_NAME}/Frameworks/${PROJECT_NAME}.xcframework" &&

rm -rf "Archive" 