#
# Be sure to run 'pod lib lint JVKit.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see https://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
    s.name             = 'JVKit'
    s.version          = '1.0.0'
    s.summary          = 'JVKit is a multi function library made for developers and help make their work easier to setup'

    s.description      = <<-DESC
    JVKit/Generics implementation of MVVM-Generics
    JVKit/Navigation implementation of ViewController presentations
    YKKit/Socket is a support for native and websocket connection to Stomp server
    DESC

    s.homepage         = 'https://bitbucket.org/0yj/jvkit-sdk/'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'Jv iOS' => 'jv.ios.developer@gmail.com' }
    s.source           = { :git => 'https://0yj@bitbucket.org/0yj/jvkit-sdk.git', :tag => s.version.to_s }

    s.swift_version     = '5.0'
    s.swift_versions    = '5.0'
    s.platform          = :ios, '13.0'
    s.ios.deployment_target = '13.0'
    
    s.vendored_frameworks = 'JVKit/Frameworks/JVKit.xcframework'
    
    s.pod_target_xcconfig = {
        'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
    }
    s.user_target_xcconfig = {
        'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64'
    }
    s.static_framework = true
    
    s.dependency 'Moya', '15.0.0'
    s.dependency 'netfox', '1.21.0'
    
end
