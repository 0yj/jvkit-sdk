//
//  SampleService.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 4/9/23.
//  Copyright © 2023 CocoaPods. All rights reserved.
//

import Foundation
import Moya
import JVKit

// MARK: - Request Enumerators
public enum SampleRequest: TargetType {
    
    case getTest
    
    public var baseURL: URL {
        return URL(string: "www.sample.come")!
    }

    public var path: String {
        return ""
    }

    public var method: Moya.Method {
        return Moya.Method.get
    }
    
    public var sampleData: Data {
        return Data()
    }
    
    public var task: Task {
        return .requestPlain
    }

    public var headers: [String: String]? {
        return [:]
    }
    
    public var validationType: ValidationType {
        return .successCodes
    }
}

class BaseService<T: TargetType> {
    
    var service: JVService<T>
    required init(provider: MoyaProvider<T>? = nil) {
        self.service = JVService(provider: provider ?? MoyaProvider<T>())
    }
}

class SampleService: BaseService<SampleRequest> {
    
    func getTest(completion: @escaping () -> Void) {
        self.service.request(void: .getTest) { result in
            switch result {
            case .success:
                break
            case .failure:
                break
            }
            completion()
        }
    }
}

extension JVServiceModel {
    var testing: SampleService {
        return SampleService()
    }
}
