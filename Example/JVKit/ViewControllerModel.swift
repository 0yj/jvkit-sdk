//
//  JVVCModel.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 4/16/22.
//

import Foundation
import UIKit
import JVKit

protocol AdditionalModel: JVModel {
    var subtitle: String? { get }
}

struct ViewControllerModel: AdditionalModel {
    
    // Required conformation
    //
    var subtitle: String? = "Subtitle"
    
    // Optional conformation
    //
    var title: String? = "Login"
}

// MARK: - Analytics
//
extension ViewControllerModel {
    enum Analytics: String, JVAnalytics {
        func name() -> String? {
            return self.rawValue
        }
        case login
    }
}

// MARK: - Routing
//
extension ViewControllerModel {
    enum Route: JVRoute {
        case login(backgroundColor: UIColor)
    }
}

extension ViewControllerModel.Route {
    
    func viewController() -> UIViewController? {
        switch self {
        case .login(let backgroundColor):
            
            let viewController2 = UIViewController()
            viewController2.view.backgroundColor = backgroundColor
            
            return viewController2
        }
    }
}
