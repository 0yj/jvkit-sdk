//
//  TestView.swift
//  JVKit_Example
//
//  Created by Joey Joseph Villafuerte on 4/16/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import JVKit
import UIKit

class TestView: JVView {
    
    var centerLabel: UILabel = {
        let centerLabel = UILabel()
        return centerLabel
    }()
    
    override func setupLayout() {
        super.setupLayout()
        
        self.centerLabel.text = #function
        self.centerLabel.textAlignment = .center
    }
    
    override func setupSubviews() {
        super.setupSubviews()
        
        self.addSubview(self.centerLabel)
    }
    
    override func setupConstraints() {
        super.setupConstraints()
        
        self.centerLabel.translatesAutoresizingMaskIntoConstraints = false
        let horizontalConstraint = self.centerLabel.centerXAnchor.constraint(equalTo: self.centerXAnchor)
        let verticalConstraint = self.centerLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor)
        let widthConstraint = self.centerLabel.widthAnchor.constraint(equalToConstant: 100)
        let heightConstraint = self.centerLabel.heightAnchor.constraint(equalToConstant: 100)
        NSLayoutConstraint.activate([horizontalConstraint, verticalConstraint, widthConstraint, heightConstraint])
    }
}
