//
//  ViewController.swift
//  JVKit
//
//  Created by Joey on 04/16/2022.
//  Copyright (c) 2022 Joey. All rights reserved.
//

import UIKit
import JVKit

struct SocketMessage: Codable {
    var owningAccountId: String
    var createDate: String
    var messageBody: String
    var senderName: String
}
// You can also use `JVBaseVCWithViewModel` just omit `ViewControllerModel`
//
class ViewController: JVVCWithViewModelAndModel<TestViewModel, ViewControllerModel> {

    var retryCounter: Int = 0
    
    // Triggere on didLoad
    //
    override func setupAnalytics(_ analytics: JVAnalytics? = nil) {
        super.setupAnalytics(analytics ?? ViewControllerModel.Analytics.login)
    }
    
    override func didDisplay(screen: JVScreen?) {
        super.didDisplay(screen: screen)
        // User the `screen` for analytics
    }
    
    // Triggere on didLoad
    //
    override func setupViewModel(_ viewModel: TestViewModel? = nil) {
        super.setupViewModel(viewModel ?? TestViewModel(parentView: self.view))
    }
    
    override func didSetViewModel() {
        super.didSetViewModel()
        
        // If needed, you can directly set viewController model like
        //
        // self.model = ViewControllerModel()
        
        JVLog.showFunction = true
        JVLog.etch(self.viewModel.testVariable, level: .info)
        
        // self.socketTesting()
    }
    
    // Triggere on willAppear
    // Note:
    //  You can omit this if you have direct setted model on didSet
    //  or else where before willAppear is triggered
    override func setup(model: ViewControllerModel?) {
        super.setup(model: model ?? ViewControllerModel())
    }
    
    override func didSetup(model: ViewControllerModel) {
        super.didSetup(model: model)
        JVLog.etch(model.title ?? "", level: .info)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // self.manualTests()
    }
    
    func manualTests() {
        
        let navigationController = self.generateTestNvigationController()
        
        self.present(navigationController, animated: false, completion: nil)
        
        // Double instance/vc of same class in navigationController.viewController
        //
        let secondRoute = ViewControllerModel.Route.login(backgroundColor: .blue)
        let secondVC = secondRoute.viewController()
        self.navigate(to: secondRoute, from: navigationController, with: .push)
        self.navigate(to: secondRoute, from: navigationController, with: .push)
        
        // Pushing the same VC from the currentVC is not allowes
        //
        self.navigate(to: secondRoute, from: secondVC, with: .push)
        
        // Presenting the same VC from the currentVC is allowes
        //
        let fourthVC = ViewControllerModel.Route.login(backgroundColor: .orange)
        self.navigate(to: fourthVC, from: navigationController, with: .present)
        
        // Setting root to navigationController is allowed
        //
        let fifthVC = ViewControllerModel.Route.login(backgroundColor: .cyan)
        self.navigate(to: fifthVC, from: navigationController, with: .root)
        
        
        // Setting root to navigationController from viewController is not allowed
        //
        let sixthVC = ViewControllerModel.Route.login(backgroundColor: .cyan)
        self.navigate(to: sixthVC, from: navigationController.viewControllers.last, with: .root)
    }
    
    func generateTestNvigationController() -> UINavigationController {
        
        let viewController1 = UIViewController()
        viewController1.view.backgroundColor = .red
        
        let navigationController = UINavigationController()
        navigationController.viewControllers = [viewController1]
        return navigationController
    }
    
    func socketTesting() {
        guard #available(iOS 13.0, *) else {
            return
        }
        let socket = JVSocket.shared
        socket.set(host: URL(string: "ws://localhost:52210")!)
        //socket.set(host: URL(string: "wss://commons-api-dev.platform-11.com/chat/websocket?testID")!)
        socket.connect(autoReconnect: true)
        socket.didConnect = { isStomp in
            guard !isStomp else {
                return
            }
            socket.subscribe(to: "/user/topic/private-messages")
        }
        socket.didDisconnect = { isStomp in
            JVLog.etch("didDisconnect: \(isStomp ? "TRUE": "FALSE")", level: .debug)
            guard !isStomp else {
                return
            }
            JVLog.etch("counter: \(self.retryCounter)", level: .debug)
            self.retryCounter += 1
            if self.retryCounter == 10 {
                socket.autoReconnect = false
            }
        }
        socket.didReceive = { message, messageId, destination, headers in
            
            guard let jsonMessage = message as? String else {
                return
            }
            do {
                let json = jsonMessage.data(using: .utf8)!
                let decoder = JSONDecoder()
                let result = try decoder.decode(SocketMessage.self, from: json)
                JVLog.etch("""
                message: \(result.owningAccountId)
                messageId: \(result.createDate)
                destination: \(result.messageBody)
                headers: \(result.senderName)
                """, level: .debug)
            } catch  {
                print(error.localizedDescription)
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
