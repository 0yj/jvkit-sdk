//
//  TestViewModel.swift
//  JVKit_Example
//
//  Created by Joey Joseph Villafuerte on 4/16/22.
//  Copyright © 2022 CocoaPods. All rights reserved.
//

import Foundation
import JVKit

class TestViewModel: JVViewModel<TestView> {
    
    var testVariable: String = "initial"
    
    override func setupLayout() {
        super.setupLayout()
        self.testVariable = #function
    }
    
    override func setupRequests() {
        super.setupRequests()
    }
}
