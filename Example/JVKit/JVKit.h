//
//  JVKit.h
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 4/8/23.
//  Copyright © 2023 CocoaPods. All rights reserved.
//

#import <Foundation/Foundation.h>

//! Project version number for JVKit.
FOUNDATION_EXPORT double JVKitVersionNumber;

//! Project version string for JVKit.
FOUNDATION_EXPORT const unsigned char JVKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <JVKit/PublicHeader.h>


