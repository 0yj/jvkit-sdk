//
//  JVScreen.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 4/16/22.
//

import Foundation

public struct JVScreen {
    public var name: String
    public let className: String
}
