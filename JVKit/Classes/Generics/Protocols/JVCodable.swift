//
//  JVCodable.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 4/16/22.
//

import Foundation

public protocol JVCodable: Codable {
    func toDictionary() -> [String: Any]
    func toData() -> Data
}

extension JVCodable {
    public func toDictionary() -> [String: Any] {
        guard let decoded = try? JSONSerialization.jsonObject(with: self.toData(), options: []) else {
            return [:]
        }
        return decoded as? [String: Any] ?? [:]
    }
    public func toData() -> Data {
        guard let jsonData = try? JSONEncoder().encode(self) else {
            return Data()
        }
        return jsonData
    }
}
