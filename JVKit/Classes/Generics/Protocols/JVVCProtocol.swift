//
//  JVVCProtocol.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 4/16/22.
//

import UIKit

public protocol JVVCProtocol {
    var rootUI: UIView {
        get
    }
    func cancelAllServices()
}
