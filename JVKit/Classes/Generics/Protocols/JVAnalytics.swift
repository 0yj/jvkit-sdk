//
//  JVAnalytics.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 4/16/22.
//

import Foundation

public protocol JVAnalytics {
    func name() -> String?
}
