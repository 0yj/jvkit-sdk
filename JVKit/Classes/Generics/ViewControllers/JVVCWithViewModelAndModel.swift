//
//  JVVCWithViewModelAndModel.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 4/16/22.
//

import Foundation

open class JVVCWithViewModelAndModel<T: JVVCProtocol, U: JVModel>: JVVCWithViewModel<T> {
    
    public var model: U?
    
    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setup(model: self.model)
    }
    
    open func setup(model: U?) {
        guard let model = model else {
            return
        }
        self.model = model
        self.didSetup(model: model)
    }
    
    open func didSetup(model: U) {
        // Intentionally unimplemented
    }
}
