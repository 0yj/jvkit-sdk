//
//  JVVCWithViewModel.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 4/16/22.
//

import Foundation
import UIKit

open class JVVCWithViewModel<T: JVVCProtocol>: JVViewController {
    
    open override func viewDidLoad() {
        super.viewDidLoad()
        self.setupAnalytics()
        self.setupViewModel()
    }
    
    // MARK: - ViewModel Setup
    //
    open func setupViewModel(_ viewModel: T? = nil) {
        guard viewModel != nil else {
            return
        }
        self.viewModel = viewModel
    }
    
    @objc open func didSetViewModel() {
        // Intentionally unimplemented
    }
    
    public var viewModel: T! {
        didSet {
            self.view = self.viewModel.rootUI
            self.didSetViewModel()
        }
    }
}
