//
//  JVViewController.swift
//  JVKit
//
//  Created by  Joseph Villafuerte on 10/2/22.
//

import Foundation
import UIKit

open class JVViewController: UIViewController {
    
    private(set) var screen: JVScreen?
    
    private var analytics: JVAnalytics? {
        didSet {
            guard let analytics = self.analytics else {
                return
            }
            self.screen = JVScreen(name: analytics.name() ?? "Unknown", className: String(describing: self.classForCoder))
            self.didDisplay(screen: self.screen)
        }
    }
    
    open func setupAnalytics(_ analytics: JVAnalytics? = nil) {
        self.analytics = analytics
    }
    
    open func didDisplay(screen: JVScreen?) {
        JVLog.etch(self.screen?.name ?? "", level: .debug)
        JVLog.etch(self.screen?.className ?? "", level: .debug)
    }
}
