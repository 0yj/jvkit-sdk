//
//  JVViewModel.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 4/16/22.
//

import UIKit

open class JVViewModel<T: JVView>: JVVCProtocol {
    
    public var rootUI: UIView
    
    public var view: T
    
    public var service: JVServiceModel
    
    public required init(
        parentView: UIView = UIView(),
        service: JVServiceModel? = nil
    ) {
        self.view = T.init(parentView: parentView)
        self.rootUI = self.view
        
        self.service = service ?? JVServiceModel()
        
        self.setupLayout()
        self.setupRequests()
    }
    // Conform JVBaseVCProtocol
    //
    public func cancelAllServices() {
        self.service.cancelAllServices()
    }
    
    // Internal functions
    //
    open func setupLayout() {
        // Intentionally empty
    }
    open func setupRequests() {
        // Intentionally empty
    }
}
