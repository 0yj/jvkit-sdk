//
//  JVView.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 4/16/22.
//

import UIKit

open class JVView: UIView {
    
    public required init(parentView: UIView = UIView()) {
        super.init(frame: parentView.frame)
        self.backgroundColor = .white
        self.setupLayout()
        self.setupSubviews()
        self.setupConstraints()
    }
    
    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

// Internal functions
//
extension JVView {
    
    @objc open func setupLayout() {
        // Intentionally empty
    }
    @objc open func setupSubviews() {
        // Intentionally empty
    }
    @objc open func setupConstraints() {
        // Intentionally unimplemented
    }
}
