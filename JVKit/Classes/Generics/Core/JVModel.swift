//
//  JVModel.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 4/16/22.
//

import Foundation

public protocol JVModel {
    var title: String? { get }
}

// Optionals
//
extension JVModel {
    public var title: String? {
        get { return nil }
        set { _ = newValue }
    }
}
