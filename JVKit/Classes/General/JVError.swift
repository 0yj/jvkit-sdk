//
//  JVError.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 7/10/22.
//

import Foundation

class JVError: Error {
    
    var code: Int = 200
    var message: String
    
    init(code: Int = 200, message: String) {
        self.code = code
        self.message = message
    }
    
    var localizedDescription: String {
        return self.message
    }
}
