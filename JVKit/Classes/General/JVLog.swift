//
//  JVLog.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 7/10/22.
//

import Foundation

public struct JVLog {
    
    static public var enable: Bool = true
    static public var showLevel: Bool = false
    static public var showLine: Bool = false
    static public var showFile: Bool = false
    static public var showFunction: Bool = false
    
    static public func etch(
        _ message: String,
        level: Level,
        line: UInt = #line,
        file: StaticString = #file,
        function: StaticString = #function
    ) {
        guard self.enable else {
            return
        }
        var details: [String] = []
        details.append({
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
            return formatter.string(from: Date())
        }())
        details.append("JVKit")
        details.append("\(level.code):")
        if self.showLevel {
            details.append("\(level)")
        }
        if self.showLine {
            details.append("\(line)")
        }
        if self.showFile {
            details.append("\(file)")
        }
        if self.showFunction {
            details.append("\(function)")
        }
        details.append(message)
        let logDetails = details.joined(separator: " ")
        if level == .info {
            print(logDetails)
            return
        }
        #if DEBUG
        print(logDetails)
        #endif
    }
}

// MARK: - JVLog Leve: `info` will be visible even in release
//
extension JVLog {
    
    public enum Level: String {
        
        case info
        case debug
        case warning
        case critical
        case error
        
        var code: String {
            switch self {
            case .info:
                return "⚪️"
            case .debug:
                return "🟣"
            case .warning:
                return "🟡"
            case .critical:
                return "🟠"
            case .error:
                return "🔴"
            }
        }
    }
}
