//
//  JVRoute.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 4/16/22.
//

import UIKit

public protocol JVRoute {
    func viewController() -> UIViewController?
}
