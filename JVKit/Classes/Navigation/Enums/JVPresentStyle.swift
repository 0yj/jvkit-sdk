//
//  JVPresentStyle.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 4/16/22.
//

import Foundation

public enum JVPresentStyle {
    case root
    case push
    case present
}
