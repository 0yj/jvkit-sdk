//
//  JVSocket+Frame.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 7/10/22.
//

import Foundation

fileprivate let NULL_CHAR = String(format: "%C", arguments: [0x00])
fileprivate let NEWLINE = "\n"
fileprivate let ENDLINE = "\0"

class JVSocketFrame<T: RawRepresentable> where T.RawValue == String {
    
    var target: T!
    var headers: [String: String] = [:]
    var body: Any?
    
    init(target: T, headers: [String: String] = [:]) {
        self.target = target
        self.headers = headers
    }
    
    init(with serializedString: String) {
        self.deserialize(string: serializedString)
    }
    
    convenience init(target: T, string: String, headers: [String: String] = [:]) {
        self.init(target: target, headers: headers)
        self.body = string
        if self.headers[JVSocket.Header.contentType.rawValue] == nil {
            self.headers[JVSocket.Header.contentType.rawValue] = "text/plain"
        }
    }
    
    convenience init(target: T, data: Data, headers: [String: String] = [:]) {
        self.init(target: target, headers: headers)
        self.body = data
    }
    
    func serialize() -> String{
        
        var frame = self.target.rawValue + NEWLINE
        
        self.headers.forEach { (key: String, value: String) in
            frame += "\(key):\(value)\(NEWLINE)"
        }
        
        if self.body != nil, let stringBody = self.body as? String {
            frame += "\(NEWLINE)\(stringBody)"
        } else if self.body != nil, let dataBody = self.body as? Data {
            let dataAsBase64 = dataBody.base64EncodedString()
            frame += "\(NEWLINE)\(dataAsBase64)"
        } else {
            frame += NEWLINE
        }
        frame += NULL_CHAR
        
        return frame
    }
    
    func deserialize(string: String) {
        
        var lines = string.components(separatedBy: NEWLINE)
        
        if lines.first == "" {
            lines.removeFirst()
        }
        
        if let command = JVSocket.Request(rawValue: lines.first ?? "") {
            self.target = (command as! T)
        } else if let command = JVSocket.Response(rawValue: lines.first ?? "") {
            self.target = (command as! T)
        } else {
            print("Invalid command")
            return
        }
        lines.removeFirst()
        
        while let line = lines.first, line != "" {
            
            let headerParts = line.components(separatedBy: ":")
            
            if headerParts.count != 2 {
                break
            }
            
            self.headers[headerParts[0].trimmingCharacters(in: .whitespacesAndNewlines)] = headerParts[1].trimmingCharacters(in: .whitespacesAndNewlines)
            
            lines.removeFirst()
        }
        
        var body = lines.joined(separator: NEWLINE)
        
        if body.hasSuffix(ENDLINE) {
            body = body.replacingOccurrences(of: ENDLINE, with: "")
        }
        
        if let data = Data(base64Encoded: body) {
            self.body = data
        } else {
            self.body = body
        }
    }
    
    func get(header: JVSocket.Header) -> String? {
        return self.headers[header.rawValue]
    }
}
