//
//  JVSocket+Header.swift
//  JVKit
//
//  Created by Joey Villafuerte on 6/15/22.
//

import Foundation

extension JVSocket {
    
    enum Status: String {
        case connecting = "Connecting"
        case connected = "Connected"
        case subscribing = "Subscribing"
        case subscribed = "Subscribed"
        case unsubscribed = "Unsubscribed"
        case reconnecting = "Reconnecting"
        case disconnected = "Disconnected"
    }
}
