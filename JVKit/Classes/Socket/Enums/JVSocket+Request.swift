//
//  JVSocket+Request.swift
//  JVKit
//
//  Created by Joey Villafuerte on 6/15/22.
//

import Foundation

extension JVSocket {
    
    enum Request: String {
        case connect = "CONNECT"
        case send = "SEND"
        case subscribe = "SUBSCRIBE"
        case unsubscribe = "UNSUBSCRIBE"
        case begin = "BEGIN"
        case commit = "COMMIT"
        case abort = "ABORT"
        case ack = "ACK"
        case nack = "NACK"
        case disconnect = "DISCONNECT"
    }
}
