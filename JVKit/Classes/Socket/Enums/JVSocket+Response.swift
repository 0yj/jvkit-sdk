//
//  JVSocket+Enum.swift
//  JVKit
//
//  Created by Joey Villafuerte on 6/15/22.
//

import Foundation

extension JVSocket {
    
    enum Response: String {
        case connected = "CONNECTED"
        case message = "MESSAGE"
        case receipt = "RECEIPT"
        case error = "ERROR"
    }
}
