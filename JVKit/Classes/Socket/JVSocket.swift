//
//  JVSocket.swift
//  JVKit
//
//  Created by Joey Villafuerte on 6/15/22.
//

import Foundation

@available(iOS 13.0, *)
public class JVSocket: NSObject, URLSessionWebSocketDelegate {
    
    public static var shared = JVSocket()
    
    public var didConnect: ((_ stomp: Bool) -> Void)?
    public var didDisconnect: ((_ stomp: Bool) -> Void)?
    public var didError: ((_ error: Error) -> Void)?
    public var didReceiveData: ((_ data: Data) -> Void)?
    public var didReceiveString: ((_ string: String) -> Void)?
    public var didReceive: ((_ message: Any?, _ messageId: String?, _ destination: String?, _ headers: [String: String]?) -> Void)?
    
    private var timeout: TimeInterval = 10
    private var heartbeatTimer: Timer?
    private var reconnectTimer: Timer?
    
    internal var status: Status = .disconnected {
        didSet {
            JVLog.etch(self.status.rawValue, level: .debug)
        }
    }
    
    public var autoReconnect: Bool = false
    public var version: String = ""
    public var socket: URLSessionWebSocketTask?
    public var host: URL?
    public var headers: [String: String] = [:]
    
    public func set(host: URL, headers: [String: String] = [:]) {
        self.host = host
        self.headers = headers
    }
    
    public func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didOpenWithProtocol protocol: String?) {
        // JVLog.log(.info, message: "\(session) \(webSocketTask) \(`protocol` ?? "")")
        self.status = .connected
        self.didConnect?(false)
    }

    public func urlSession(_ session: URLSession, webSocketTask: URLSessionWebSocketTask, didCloseWith closeCode: URLSessionWebSocketTask.CloseCode, reason: Data?) {
        // JVLog.log(.info, message: "\(session) \(webSocketTask) \(closeCode) \(reason ?? Data())")
        self.status = .disconnected
        self.didDisconnect?(false)
    }
}

@available(iOS 13.0, *)
extension JVSocket {
    
    public func connect(timeout: TimeInterval = 10, acceptedVersion: String = "1.1,1.2", autoReconnect: Bool = false) {
        
        self.status = .connecting
        
        self.timeout = timeout
        self.version = acceptedVersion
        self.autoReconnect = autoReconnect
        
        guard let host = self.host else {
            return
        }
        
        var request = URLRequest(url: host)
        request.timeoutInterval = self.timeout
        self.headers.forEach { (key: String, value: String) in
            request.addValue(value, forHTTPHeaderField: key)
        }
        
        let session = URLSession(configuration: .default, delegate: self, delegateQueue: OperationQueue())
        self.socket = session.webSocketTask(with: request)
        self.socket?.resume()
        
        self.setReceiver()
        
        self.keepAlive()
        
        self.stompConnect()
    }
    
    public func disconnect() {
        self.socket?.cancel(with: .goingAway, reason: nil)
        self.socket = nil
        
        self.stompDisconnect()
        
        self.keepAliveInvalidate()
    }
}

@available(iOS 13.0, *)
extension JVSocket {
    
    public func send(data: Data) {
        self.socket?.send(
            URLSessionWebSocketTask.Message.data(data), completionHandler: { error in
                guard let error = error else {
                    return
                }
                self.didError?(error)
            }
        )
    }
    
    public func send(message: String) {
        self.socket?.send(
            URLSessionWebSocketTask.Message.string(message), completionHandler: { error in
                guard let error = error else {
                    return
                }
                self.didError?(error)
            }
        )
    }
        
    public func send(payload: [String: String], asData: Bool = true) {
        guard let jsonData = try? JSONSerialization.data(withJSONObject: payload, options: []) else {
            return
        }
        if asData {
            self.send(data: jsonData)
        } else {
            self.send(message: String(data: jsonData, encoding: .utf8) ?? "")
        }
    }
}

@available(iOS 13.0, *)
extension JVSocket {
    
    private func setReceiver() {
        self.socket?.receive(completionHandler: { [unowned self] result in
            switch result {
            case .failure(let error):
                self.didError?(error)
                self.status = .disconnected
                self.didDisconnect?(false)
                self.disconnect()
                self.reconnect()
            case .success(let data):
                self.handleSocket(data)
            }
            self.setReceiver()
        })
    }
    
    private func handleSocket(_ data: URLSessionWebSocketTask.Message) {
        switch data {
        case .data(let data):
            self.didReceiveData?(data)
        case .string(let string):
            guard !self.stomeReceive(string: string) else {
                return
            }
            self.didReceiveString?(string)
        @unknown default:
            JVLog.etch("Unsupported case", level: .debug)
        }
    }
}

@available(iOS 13.0, *)
extension JVSocket {
    
    private func keepAlive() {
        self.heartbeatTimer = Timer.scheduledTimer(withTimeInterval: 10.0, repeats: true) { timer in
            self.socket?.sendPing(pongReceiveHandler: { error in
                guard let error = error else {
                    return
                }
                JVLog.etch(error.localizedDescription, level: .error)
                self.keepAliveInvalidate()
            })
        }
        self.heartbeatTimer?.fire()
    }
    
    private func keepAliveInvalidate() {
        self.heartbeatTimer?.invalidate()
        self.heartbeatTimer = nil
    }
}

@available(iOS 13.0, *)
extension JVSocket {
    
    private func reconnect() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
            guard self.autoReconnect else {
                return
            }
            self.status = .reconnecting
            self.connect(
                timeout: self.timeout,
                acceptedVersion: self.version,
                autoReconnect: self.autoReconnect
            )
        }
    }
    
    private func reconnectInvalidate() {
        self.reconnectTimer?.invalidate()
        self.reconnectTimer = nil
    }
}
