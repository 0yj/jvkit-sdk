//
//  JVSocket+Stomp.swift
//  JVKit
//
//  Created by Joey Joseph Villafuerte on 7/10/22.
//

import Foundation

@available(iOS 13.0, *)
extension JVSocket {
    
    func stomeReceive(string: String) -> Bool {
        
        let frame: JVSocketFrame<JVSocket.Response> = JVSocketFrame.init(with: string)
        print("""
        \(frame.target.rawValue)
        \(frame.headers)
        """)
        switch frame.target {
        case .connected:
            self.status = .subscribed
            self.didConnect?(true)
        case .message:
            let messageId = frame.get(header: .messageId) ?? ""
            let destination = frame.get(header: .destination) ?? ""
            self.didReceive?(frame.body, messageId, destination, self.headers)
        case .receipt:
            guard let receiptId = frame.get(header: .receiptId), receiptId == "disconnect/safe" else {
                return false
            }
            self.status = .disconnected
            self.didDisconnect?(true)
        case .error:
            self.didError?(JVError(
                code: 500,
                message: "P11Socket: Internal error"
            ))
            break
        default:
            // Intentionally unimplemented
            break
        }
        guard [.connected, .message, .receipt, .error].contains(frame.target) else {
            return true
        }
        
        return false
    }
}

@available(iOS 13.0, *)
extension JVSocket {
    
    func stompConnect() {
        self.send(message: JVSocketFrame(
            target: JVSocket.Request.connect,
            headers: self.generate(headers: [
                Header.acceptVersion.rawValue: self.version
            ])
        ).serialize())
    }
    
    func stompDisconnect() {
        self.send(message: JVSocketFrame(
            target: JVSocket.Request.disconnect,
            headers: self.generate(headers: [
                Header.receipt.rawValue: "disconnect/safe"
            ])
        ).serialize())
    }
    
    public func subscribe(to destination: String) {
        self.status = .subscribing
        self.send(message: JVSocketFrame(
            target: Request.subscribe,
            headers: self.generate(headers: [
                Header.id.rawValue: destination,
                Header.destination.rawValue: destination,
                Header.ack.rawValue: "auto"
            ])
        ).serialize())
    }
    
    public func unsubscribe(to destination: String) {
        self.status = .unsubscribed
        self.send(message: JVSocketFrame(
            target: Request.unsubscribe,
            headers: self.generate(headers: [
                Header.id.rawValue: destination
            ])
        ).serialize())
    }
    
    public func send(message: String, to: String, receiptId: String? = nil, headers: [String: String]? = nil) {
        self.send(message: JVSocketFrame(
            target: Request.send,
            string: message,
            headers: self.generate(
                headers: self.sendHeader(to: to, receiptId: receiptId, headers: headers)
            )
        ).serialize())
    }
    
    public func send(data: Data, to: String, receiptId: String? = nil, headers: [String: String]? = nil) {
        self.send(message: JVSocketFrame(
            target: Request.send,
            data: data,
            headers: self.generate(
                headers: self.sendHeader(to: to, receiptId: receiptId, headers: headers)
            )
        ).serialize())
    }
    
    private func sendHeader(to: String, receiptId: String?, headers: [String: String]?) -> [String: String] {
        var messageHeader: [String: String] = [
            Header.destination.rawValue: to
        ]
        if let receiptId = receiptId {
            messageHeader[Header.receipt.rawValue] = receiptId
        }
        return messageHeader
    }
    
    private func generate(headers: [String: String]) -> [String: String] {
        headers.forEach { (key, value) in
            self.headers[key] = value
        }
        return self.headers
    }
}
