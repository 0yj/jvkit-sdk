//
//  JVPinning.swift
//  JVKit
//
//  Created by Joey Villafuerte on 6/15/22.
//

import Foundation
import Security
import CommonCrypto

public class JVPinning: NSObject {
    
    public static var shared = JVPinning()
    
    public var enabled: Bool = true
    public var cached: Bool = false
    public var status: Bool?
    public var host: URL?
    public var keys: [String] = []
    
    public func evaluate(enabled: Bool = true, cached: Bool = true, host url: URL, keys: [String], completion: @escaping (Bool) -> Void) {
        self.enabled = enabled
        self.cached = cached
        self.host = url
        self.keys = keys
        guard enabled else {
            completion(!enabled)
            return
        }
        if self.status != nil, cached {
            completion(self.status ?? false)
            return
        }
        let session = URLSession(configuration: .ephemeral, delegate: self, delegateQueue: nil)
        let task = session.dataTask(with: url) { (data, _, error) in
            completion(error == nil && data != nil)
        }
        task.resume()
    }
}

// MARK: - Server keys extraction
//
extension JVPinning {
    
    private func sha256(data: Data, header: [UInt8]) -> String {
        var keyWithHeader = Data(header)
        keyWithHeader.append(data)
        var hash = [UInt8](repeating: 0, count: Int(CC_SHA256_DIGEST_LENGTH))
        keyWithHeader.withUnsafeBytes {
            hash.append(contentsOf: $0)
        }
        return Data(hash).base64EncodedString()
    }
    
    private func extractKey(from data: Data) -> [String] {
        var sha256s: [String] = []
        Headers.allCases.forEach {
            sha256s.append(self.sha256(
                data: data,
                header: $0.n1Header
            ))
        }
        return sha256s
    }
    
    private func isInExtractedKeys(from data: Data) -> Bool {
        for key in self.extractKey(from: data) where self.keys.contains(key) {
            return true
        }
        return false
    }
}

// MARK: - URLSessionDelegate
//
extension JVPinning: URLSessionDelegate {
    
    public func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {
        guard #available(iOS 12.0, *),
              let serverTrust = challenge.protectionSpace.serverTrust,
              let serverCertificate = SecTrustGetCertificateAtIndex(serverTrust, 0)
        else {
            completionHandler(.cancelAuthenticationChallenge, nil)
            return
        }
        let serverPublicKey = SecCertificateCopyKey(serverCertificate)
        let serverPublicKeyData = SecKeyCopyExternalRepresentation(serverPublicKey!, nil )!
        let data: Data = serverPublicKeyData as Data
        let isInExtractedKeys = self.isInExtractedKeys(from: data)
        if self.status != isInExtractedKeys {
            self.status = isInExtractedKeys
        }
        if self.status ?? false {
            completionHandler(.useCredential, URLCredential(trust: serverTrust))
        } else {
            completionHandler(.cancelAuthenticationChallenge, nil)
        }
    }
}

extension JVPinning {
    
    private enum Headers: CaseIterable {
        
        case rsa2048
        case rsa4096
        case ecDsaSecp256r1
        case ecDsaSecp384r1
        
        var n1Header: [UInt8] {
            switch self {
            case .rsa2048:
                return [
                    0x30, 0x82, 0x01, 0x22, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86,
                    0xf7, 0x0d, 0x01, 0x01, 0x01, 0x05, 0x00, 0x03, 0x82, 0x01, 0x0f, 0x00
                ]
            case .rsa4096:
                return [
                    0x30, 0x82, 0x02, 0x22, 0x30, 0x0d, 0x06, 0x09, 0x2a, 0x86, 0x48, 0x86,
                    0xf7, 0x0d, 0x01, 0x01, 0x01, 0x05, 0x00, 0x03, 0x82, 0x02, 0x0f, 0x00
                ]
            case .ecDsaSecp256r1:
                return [
                    0x30, 0x59, 0x30, 0x13, 0x06, 0x07, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x02,
                    0x01, 0x06, 0x08, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x03, 0x01, 0x07, 0x03,
                    0x42, 0x00
                ]
            case .ecDsaSecp384r1:
                return [
                    0x30, 0x76, 0x30, 0x10, 0x06, 0x07, 0x2a, 0x86, 0x48, 0xce, 0x3d, 0x02,
                    0x01, 0x06, 0x05, 0x2b, 0x81, 0x04, 0x00, 0x22, 0x03, 0x62, 0x00
                ]
            }
        }
    }
}
