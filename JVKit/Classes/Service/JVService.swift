//
//  JVService.swift
//  JVKit
//
//  Created by Joey Villafuerte on 12/04/22.
//

import Foundation
import Moya

public class JVService<T: TargetType> {
    
    private(set) var provider: MoyaProvider<T>
    
    public init(provider: MoyaProvider<T>) {
        self.provider = provider
        if #available(iOS 13.0, *) {
            self.provider.session.sessionConfiguration.allowsConstrainedNetworkAccess = false
        }
    }
    
    public func request(void request: T, completion: @escaping (Swift.Result<Void, Error>) -> Void) {
        self.execute(request) { result in
            switch result {
            case .success:
                completion(.success(()))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    public func request(data request: T, completion: @escaping (Swift.Result<Data, Error>) -> Void) {
        self.execute(request) { result in
            switch result {
            case .success(let response):
                completion(.success(response.data))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    public func request(bool request: T, completion: @escaping (Swift.Result<Bool, Error>) -> Void) {
        self.execute(request) { result in
            switch result {
            case .success(let response):
                let okResponse = [
                    200, 201
                ].contains(response.statusCode)
                completion(.success(okResponse))
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    public func request<U: Decodable>(required request: T, type: U.Type, completion: @escaping (Swift.Result<U, Error>) -> Void) {
        self.execute(request) { result in
            switch result {
            case .success(let response):
                do {
                    let decoded = try JSONDecoder().decode(U.self, from: response.data)
                    completion(.success(decoded))
                } catch let error {
                    completion(.failure(error))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    public func request<U: Decodable>(optional request: T, type: U.Type, completion: @escaping (Swift.Result<U?, Error>) -> Void) {
        self.execute(request) { result in
            switch result {
            case .success(let response):
                do {
                    let decoded = try JSONDecoder().decode(U.self, from: response.data)
                    completion(.success(decoded))
                } catch {
                    completion(.success(nil))
                }
            case .failure(let error):
                completion(.failure(error))
            }
        }
    }
    
    public func cancelAllRequest() {
        self.provider.session.cancelAllRequests()
    }
}

// Other functions
//
extension JVService {
    
    private func execute(_ request: T, completion: @escaping (Result<Response, MoyaError>) -> Void) {
        self.validate(request) { status, error in
            if status {
                self.provider.request(request, completion: completion)
                return
            } else if let moya = error as? MoyaError {
                completion(Swift.Result.failure(moya))
                return
            }
            completion(Swift.Result.failure(
                MoyaError.statusCode(Response(
                    statusCode: 511, // Network Authentication Required
                    data: Data()
                ))
            ))
        }
    }
    
    private func validate(_ request: T, completion: @escaping (Bool, Error?) -> Void) {
        // Add request validation here
        // ex. SSL Pinning, etc.
        //
        let shared = JVPinning.shared
        shared.evaluate(
            enabled: shared.enabled,
            cached: shared.cached,
            host: request.baseURL,
            keys: shared.keys
        ) { evaluation in
            completion(evaluation, nil)
        }
    }
}
