# Make this executable using:
# > chmod u+x xcpods.sh
# Execute it using:
# ./xcpods.sh lint
#
# lint if just for linting the podspec, dont push any if push
#
SDK_PODSPEC="JVKit.podspec"

if [ $1 == "lint" ]; then
	# Checking if sdk is valid
	pod lib lint ${SDK_PODSPEC} --verbose
elif [ $1 == "push" ]; then
	# Publishing sdk:
	pod trunk push "${SDK_PODSPEC}" --verbose
elif [ $1 == "clean" ]; then
	# Clean pod cache:
	pod cache clean --all --verbose && rm -f Example/Podfile.lock
elif [ $1 == "build" ]; then
	# Building universal framework
	./xcframework.sh && ./xcpods.sh clean && cd "Example" && pod install --repo-update
elif [ $1 == "install" ]; then
	# Installing pods:
	cd "Example" && pod install --repo-update
elif [ $1 == "reinstall" ]; then
	# Installing pods:
	cd "Example" && pod deintegrate && pod install --repo-update
elif [ $1 == "release" ]; then
	# Releasing pods:
	# ex.
	# ./xcpods.sh release dev1.0.0
	#
	./xcframework.sh && ./xcpods.sh clean && git add . && git commit -m "Releasing $2" && git tag $2 && git push origin $2 && ./xcpods.sh push
else 
	echo "Unknown command"
fi