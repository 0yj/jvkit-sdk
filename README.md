# JVKit

[![CI Status](https://img.shields.io/travis/Joey/JVKit.svg?style=flat)](https://travis-ci.org/Joey/JVKit)
[![Version](https://img.shields.io/cocoapods/v/JVKit.svg?style=flat)](https://cocoapods.org/pods/JVKit)
[![License](https://img.shields.io/cocoapods/l/JVKit.svg?style=flat)](https://cocoapods.org/pods/JVKit)
[![Platform](https://img.shields.io/cocoapods/p/JVKit.svg?style=flat)](https://cocoapods.org/pods/JVKit)

JVKit is a multi function library made for developers and help make their work easier to setup. It provides base and reuseable classes as template.

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## General

`JVError` an inheritance from `Error` protocol of apple; a class containing just two(2) properties.

- code: Int = 200
- message: String

`JVLog` for logging that is wrapped with:
```c
#if DEBUG
...
#endif
```
Note: Everything logged using JVLog will not be visible in Release/Production build

`JVLog.Level` is used for log type identification.

- ⚪️ info 
- 🟣 debug 
- 🟡 warning 
- 🟠 critical 
- 🔴 error 

```swift
JVLog.etch("This is an example log", level: .debug)

// 2023-04-09 11:21:53 JVKit 🟣: This is an example log
```

## Generics

Contains all the base classes for MVVM-Generics implementation

- **Core**
	- `JVModel` - A protocol used for view controller model configuration
	- `JVView` - A subclass of UIView, used as the `.view`(rootView or baseClass) of the viewModel
	- `JVViewModel` - Declared with generic JVView and conforms `JVVCProtocol`
	- `JVViewController` - A subclass of UIViewController, baseClass for view controllers



## CocoaPods

To integrate JVKit into your Xcode project using [cocoapods](https://cocoapods.org), simply add the following line to your Podfile:

```ruby
pod 'JVKit'
```

## License

JVKit is available under the MIT license. See the LICENSE file for more info.
